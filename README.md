# otus.hw.react 1. Базовый сетап фронтенд части проекта с React

**Цель:** Научиться разворачивать фронтенд проекты с npm, webpack, React;

Создать проект с create-react-app;

### Критерии оценки:

4 балла: Создан репозиторий с package.json;  
2 балла: Добавлен React проект;  

Минимальный проходной балл: 6

# otus.hw.react 2. Получение данных в React приложении

**Цель:** Разобраться с настройками webpack/create-react-app;

### Подключить axios, добавить вызов АПИ

1. Расширить конфиг добавив проксирование вызова АПИ;
2. Написать компонент с формой (например авторизации) отправляющий запрос.
3. Установить axios

### Критерии оценки: 
2 балла: Eсть конфиг и проксирование вызова АПИ  
2 балла: Есть форма  
2 балла: Есть вызов АПИ;  

# otus.hw.react 3. Роутинг и управление стейтом с React

**Цель:** Научиться писать сложный фронтенд с роутингом и управлением стейтом

### Разобраться с подключением сторонних плагинов и UI компонентов

1. Установить react-router;
2. Добавить отдельные компоненты страниц - Login / Register / HomePage / 404;
3. Добавить стейт-менеджемент с Redux;
4. Найти возможное дублирование кода и применить HOC паттерн.

### Критерии оценки: 
4 балла:В React проект добавлен react-router  
2 балла: В React проект добавлен Redux;  
2 балла: В React проект добавлена библиотека UI компонентов (Bootstrap / material design);  
1 балл: Сделано 2 CodeReview;  
1 балл: CodeStyle, грамотная архитектура, все замечания проверяющего исправлены.  
