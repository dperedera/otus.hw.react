import './App.css';
import Register from './Components/Register';
import Login from './Components/Login';
import NotFound from './Components/NotFound';
import HomePage from './Components/HomePage';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Button from '@material-ui/core/Button';


import {
  BrowserRouter as Router,
  Switch,
  Route,
  Link
} from "react-router-dom";

function App() {
  return (
    <Router>
      <AppBar position="static">
        <Toolbar>
          <Button variant="contained">
            <Link to="/">Home</Link>
            </Button>
          <Button variant="contained">
          <Link to="/login">Login</Link>
            </Button>
          <Button variant="contained">
          <Link to="/register">Register</Link>
            </Button>
        </Toolbar>
      </AppBar>

      <Switch>
        <Route path="/login">
          <Login />
        </Route>
        <Route path="/register">
          <Register />
        </Route>
        <Route exact path="/">
          <HomePage />
        </Route>
        <Route path="*">
          <NotFound />
        </Route>
      </Switch>
    </Router>);
}

export default App;
