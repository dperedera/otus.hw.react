import './../App.css';
import React from 'react';
import Slider from '@material-ui/core/Slider';
import axios from 'axios';

export default class OtusHwReactAxiosComponent extends React.Component {
    constructor(props) {
        super(props);
        this.state = {};
    }

    getAnswer(value) {
        var answer = 'Slide it to get answer!';
        axios.get('/answers/' + value)
            .then((response) => {
                answer = response.data.answer;
                console.log(response.data.answer);
            });
        return answer;
    }

    render() {
        return (
            <div className="container-content">
                <Slider
                    defaultValue={1}
                    getAriaValueText={this.getAnswer}
                    aria-labelledby="discrete-slider"
                    valueLabelDisplay="auto"
                    step={1}
                    marks={true}
                    min={1}
                    max={5}
                />

            </div>
        );
    }
}