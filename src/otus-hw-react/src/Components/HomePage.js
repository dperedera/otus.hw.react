import lizard from './../img/lizard.jpg';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardActions from '@material-ui/core/CardActions';
import CardContent from '@material-ui/core/CardContent';
import CardMedia from '@material-ui/core/CardMedia';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
const useStyles = makeStyles({
    root: {
        maxWidth: 345,
    },
    media: {
        height: 140,
    },
});

function HomePage() {
    const classes = useStyles();

    return (
        <div>
	<h1>HomePage</h1>
	<Card className={classes.root}>
		<CardActionArea>
			<CardMedia className={classes.media} image={lizard} title="Contemplative Reptile"/>
			<CardContent>
				<Typography gutterBottom variant="h5" component="h2">
					Lizard 
				</Typography>
				<Typography variant="body2" color="textSecondary" component="p">
                    <strong>Just a water, instead of Lorem Ipsum.</strong><br/>
					Lizards are a widespread group of squamate reptiles, with over 6,000 species, ranging
					across all continents except Asntarctica
				</Typography>
			</CardContent>
		</CardActionArea>
	</Card>
    
</div>

    )
}

export default HomePage;