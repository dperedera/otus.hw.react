import notFound from './../img/404-2.png';
function NotFound() {
    return(
        <div align="center">
            <img src={notFound} alt="Error 404. Page not found." />
        </div>
    )
}

export default NotFound;